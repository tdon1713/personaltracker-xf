﻿using PersonalTrackerXF.Helper;
using System.Threading.Tasks;

namespace PersonalTrackerXF.ServiceInterfaces
{
    public interface IFileHelper
    {
        Task ClearFolder(Subfolders folder);

        Task<(bool success, string filePath)> Download(string url, Subfolders subFolder, SupportedFileType fileType);

        string GetRoot();

        Task<string> GetPath(Subfolders subFolder);

        Task<string> Move(string location, Subfolders subFolder);
    }
}
