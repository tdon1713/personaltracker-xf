﻿using PersonalTrackerXF.ViewModels;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PersonalTrackerXF.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SettingsPage : ContentPage
	{
        #region Public Properties

        public SettingsPageViewModel ViewModel
        {
            get => (SettingsPageViewModel)BindingContext;
            set => BindingContext = value;
        }

        #endregion

        public SettingsPage ()
		{
            InitializeComponent ();
            ViewModel = new SettingsPageViewModel();
		}

        #region Override Events

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            await ViewModel.InitAsync();
        }

        #endregion
    }
}