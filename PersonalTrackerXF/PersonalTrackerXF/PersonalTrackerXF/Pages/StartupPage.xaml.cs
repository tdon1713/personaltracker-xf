﻿using PersonalTrackerXF.ViewModels;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PersonalTrackerXF.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class StartupPage : ContentPage
	{
        #region Public Properties

        public StartupPageViewModel ViewModel
        {
            get { return BindingContext as StartupPageViewModel; }
            set { BindingContext = value; }
        }

        #endregion

        #region Constructor

        public StartupPage()
        {
            InitializeComponent();
            ViewModel = new StartupPageViewModel();
        }

        #endregion

        #region Override Methods

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            await ViewModel.InitAsync();
        }

        #endregion
    }
}