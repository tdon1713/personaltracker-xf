﻿using PersonalTrackerXF.Helper;
using PersonalTrackerXF.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PersonalTrackerXF.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainMasterPage : MasterDetailPage
    {
        public MainMasterPageViewModel ViewModel
        {
            get => BindingContext as MainMasterPageViewModel;
            set => BindingContext = value;
        }

        public MainMasterPage()
        {
            InitializeComponent();
            ViewModel = new MainMasterPageViewModel();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            await ViewModel.InitAsync();
        }
    }
}