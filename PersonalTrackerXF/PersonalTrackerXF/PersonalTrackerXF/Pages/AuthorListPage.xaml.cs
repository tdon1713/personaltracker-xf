﻿using PersonalTrackerXF.ViewModels;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PersonalTrackerXF.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AuthorListPage : ContentPage
	{
        public AuthorListPageViewModel ViewModel
        {
            get => BindingContext as AuthorListPageViewModel;
            set => BindingContext = value;
        }

		public AuthorListPage ()
		{
			InitializeComponent ();
            ViewModel = new AuthorListPageViewModel();
		}

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            await ViewModel.InitAsync();
        }
    }
}