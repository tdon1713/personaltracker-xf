﻿using PersonalTrackerXF.ViewModels;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PersonalTrackerXF.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class BookListPage : ContentPage
	{
        public BookListPageViewModel ViewModel
        {
            get => BindingContext as BookListPageViewModel;
            set => BindingContext = value;
        }

		public BookListPage ()
		{
			InitializeComponent ();
            ViewModel = new BookListPageViewModel();
		}

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            await ViewModel.InitAsync();
        }
    }
}