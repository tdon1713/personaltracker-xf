﻿using PersonalTrackerXF.ViewModels;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PersonalTrackerXF.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class OverviewPage : ContentPage
	{
        public OverviewPageViewModel ViewModel
        {
            get => BindingContext as OverviewPageViewModel;
            set => BindingContext = value;
        }

		public OverviewPage ()
		{
			InitializeComponent ();
            ViewModel = new OverviewPageViewModel();
		}

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            await ViewModel.InitAsync();
        }
    }
}