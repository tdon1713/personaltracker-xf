﻿using PersonalTrackerXF.ViewModels;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PersonalTrackerXF.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AuthorItemPage : ContentPage
	{
        public AuthorItemPageViewModel ViewModel
        {
            get => BindingContext as AuthorItemPageViewModel;
            set => BindingContext = value;
        }

		public AuthorItemPage (AuthorListItemViewModel author = null)
		{
			InitializeComponent ();
            ViewModel = new AuthorItemPageViewModel(author);
		}

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            await ViewModel.InitAsync();
        }
    }
}