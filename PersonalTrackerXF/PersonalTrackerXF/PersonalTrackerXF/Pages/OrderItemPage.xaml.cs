﻿using PersonalTrackerXF.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PersonalTrackerXF.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OrderItemPage : ContentPage
    {
        public OrderItemPageViewModel ViewModel
        {
            get => BindingContext as OrderItemPageViewModel;
            set => BindingContext = value;
        }

        public OrderItemPage(OrderListItemViewModel order = null)
        {
            InitializeComponent();
            ViewModel = new OrderItemPageViewModel(order);
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            await ViewModel.InitAsync();
        }
    }
}