﻿using PersonalTrackerXF.Helper;
using PersonalTrackerXF.ViewModels;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PersonalTrackerXF.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BookItemPage : ContentPage
    {
        public BookItemPageViewModel ViewModel
        {
            get => BindingContext as BookItemPageViewModel;
            set => BindingContext = value;
        }

        public BookItemPage(BookListItemViewModel book = null)
        {
            InitializeComponent();
            ViewModel = new BookItemPageViewModel(book);
        }
        
        protected override async void OnAppearing()
        {
            base.OnAppearing();
            await ViewModel.InitAsync();
        }
    }
}