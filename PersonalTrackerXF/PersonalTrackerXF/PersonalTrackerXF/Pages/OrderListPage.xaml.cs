﻿using PersonalTrackerXF.ViewModels;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PersonalTrackerXF.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class OrderListPage : ContentPage
	{
        public OrderListPageViewModel ViewModel
        {
            get => BindingContext as OrderListPageViewModel;
            set => BindingContext = value;
        }

		public OrderListPage ()
		{
			InitializeComponent ();
            ViewModel = new OrderListPageViewModel();
		}

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            await ViewModel.InitAsync();
        }
    }
}