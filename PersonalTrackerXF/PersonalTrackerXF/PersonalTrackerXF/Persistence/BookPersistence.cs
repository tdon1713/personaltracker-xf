﻿using PersonalTrackerXF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonalTrackerXF.Persistence
{
    public class BookPersistence : BasePersistence<Book>
    {
        public async Task DeleteAsync(Author author)
        {
            await Database.QueryAsync<Book>("DELETE FROM Books where AuthorID = ?", author.ID);
        }
        
        public new async Task DeleteAsync(Book book)
        {
            await base.DeleteAsync(book);

            var author = await base.GetAsync<Author>(book.AuthorID);
            if (author != null)
            {
                author.BookCount--;
                await base.SaveAsync<Author>(author);
            }
        }

        public new async Task<List<Book>> GetAsync()
        {
            var query = from book in await Database.Table<Book>().ToListAsync()
                        orderby book.Status, book.DateCompleted descending, book.Title, book.AuthorID, book.Volume
                        join author in await Database.Table<Author>().ToListAsync() on book.AuthorID equals author.ID into authors
                        from author in authors.DefaultIfEmpty()
                        select new Book(book, author);

            return query.ToList();
        }

        public async Task<List<Book>> GetForAuthorAsync(int id)
        {
            return await Database.Table<Book>().Where(item => item.AuthorID == id).ToListAsync();
        }
    }
}
