﻿using PersonalTrackerXF.Models;
using PersonalTrackerXF.ServiceInterfaces;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PersonalTrackerXF.Persistence
{
    public class BasePersistence
    {
        private static bool _initialized = false;
        private static string _dbPath;//C:\Users\tyler\AppData\Local\Packages\debde612-121f-45b6-8e30-594cee921b1d_2qnhh2ntn4060\LocalState
        private static string _dbName = "tracker.db3";

        protected static SQLiteAsyncConnection Database;

        public static void Init()
        {
            if (_initialized)
                return;

            var tempPath = DependencyService.Get<IFileHelper>().GetRoot();
            _dbPath = Path.Combine(tempPath, _dbName);
            Database = new SQLiteAsyncConnection(_dbPath, storeDateTimeAsTicks: false);

            Database.CreateTableAsync<AppSettings>().Wait();
            Database.CreateTableAsync<Author>().Wait();
            Database.CreateTableAsync<Book>().Wait();
            Database.CreateTableAsync<Order>().Wait();
            Database.CreateTableAsync<OrderItem>().Wait();
            Database.CreateTableAsync<Video>().Wait();

            _initialized = true;
        }
    }

    public class BasePersistence<T> : BasePersistence where T : BaseModel, new()
    {
        public async Task<int> DeleteAsync(T item)
        {
            return await Database.DeleteAsync(item);
        }

        public async Task<int> DeleteAsync(int id)
        {
            var item = await Database.FindAsync<T>(id);
            if (item != null)
                return await Database.DeleteAsync(item);

            return -1;
        }

        public async Task<T> GetAsync(int id)
        {
            return await Database.FindAsync<T>(i => i.ID == id);
        }

        public async Task<R> GetAsync<R>(int id) where R : BaseModel, new()
        {
            return await Database.FindAsync<R>(r => r.ID == id);
        }

        public async Task<List<T>> GetAsync()
        {
            return await Database.Table<T>().ToListAsync();
        }
        
        public async Task<T> SaveAsync(T item)
        {
            if (item.ID == 0)
            {
                await Database.InsertAsync(item);
            }
            else
            {
                await Database.UpdateAsync(item);
            }

            return item;
        }

        public async Task<R> SaveAsync<R>(R item) where R : BaseModel, new()
        {
            if (item.ID == 0)
            {
                item.ID = await Database.InsertAsync(item);
            }
            else
            {
                await Database.UpdateAsync(item);
            }

            return item;
        }
    }
}
