﻿using PersonalTrackerXF.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PersonalTrackerXF.Persistence
{
    public class OrderItemPersistence : BasePersistence<OrderItem>
    {
        public async Task DeleteAsync(Order order)
        {
            await Database.QueryAsync<OrderItem>("DELETE FROM OrderItems where OrderID = ?", order.ID);
        }

        public async Task<IEnumerable<OrderItem>> GetItemsAsync(int orderId)
        {
            return await Database.Table<OrderItem>().Where(item => item.OrderID == orderId).ToListAsync();
        }
    }
}
