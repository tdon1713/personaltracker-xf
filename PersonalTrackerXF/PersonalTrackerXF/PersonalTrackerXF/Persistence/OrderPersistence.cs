﻿using PersonalTrackerXF.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonalTrackerXF.Persistence
{
    public class OrderPersistence : BasePersistence<Order>
    {
        public new async Task<List<Order>> GetAsync()
        {
            var query = from order in await Database.Table<Order>().ToListAsync()
                        orderby order.ArrivalDate
                        join orderItem in await Database.Table<OrderItem>().ToListAsync() on order.ID equals orderItem.OrderID into orderItems
                        select new Order(order, orderItems);

            return query.ToList();
        }

        public new async Task<Order> GetAsync(int id)
        {
            var query = from order in await Database.Table<Order>().ToListAsync()
                        where order.ID == id
                        join orderItem in await Database.Table<OrderItem>().ToListAsync() on order.ID equals orderItem.OrderID into orderItems
                        select new Order(order, orderItems);

            return query.FirstOrDefault();
        }
    }
}
