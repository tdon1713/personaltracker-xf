using PersonalTrackerXF.Pages;
using PersonalTrackerXF.ServiceInterfaces;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace PersonalTrackerXF
{
    public partial class App : Application
	{
		public App ()
		{
			InitializeComponent();

			MainPage = new StartupPage();
		}

		protected override void OnStart ()
		{
            // Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
