﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace PersonalTrackerXF.Converters
{
    public class IsNotNullConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (DateTime.TryParse(value?.ToString(), out DateTime result))
            {
                return (result as DateTime?) != DateTime.MinValue;
            }

            return value != null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is DateTime)
            {
                return !((value as DateTime?) != DateTime.MinValue);
            }
            return !(value == null);
        }
    }
}
