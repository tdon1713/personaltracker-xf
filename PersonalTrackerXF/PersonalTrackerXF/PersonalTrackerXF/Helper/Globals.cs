﻿using PersonalTrackerXF.Logic;
using PersonalTrackerXF.Models;
using System.Threading.Tasks;

namespace PersonalTrackerXF.Helper
{
    public sealed class Globals
    {
        private static bool _initialized;
        private static Globals _globals;
        private static readonly object _lock = new object();

        public static Globals Instance
        {
            get
            {
                lock(_lock)
                {
                    if (_globals == null)
                    {
                        _globals = new Globals();
                    }
                }

                return _globals;
            }
        }

        public AppSettings Settings { get; set; }

        public static async Task InitAsync()
        {
            if (_initialized)
                return;

            _globals = new Globals();
            _globals.Settings = await AppSettingsLogic.Get();

            if (_globals.Settings == null)
            {
                _globals.Settings = await AppSettingsLogic.Insert();
            }

            _initialized = true;
        }
    }
}
