﻿using System.Threading.Tasks;
using Xamarin.Forms;

namespace PersonalTrackerXF.Helper
{
    public static class Paging
    { 
        public static async Task<bool> DisplayAlert(string title, string message, string ok, string cancel)
        {
            return await Application.Current.MainPage.DisplayAlert(title, message, ok, cancel);
        }

        public static async Task PopDetailAsync()
        {
            await (Application.Current.MainPage as MasterDetailPage).Detail.Navigation.PopAsync();
        }

        public static async Task PopModalAsync()
        {
            await Application.Current.MainPage.Navigation.PopModalAsync();
        }

        public static void MoveTo(Page page)
        {
            Application.Current.MainPage = page;
        }

        public static async Task PushAsync(Page page)
        {
            await Application.Current.MainPage.Navigation.PushAsync(page);
        }

        public static async Task PushDetailAsync(Page page)
        {
            await (Application.Current.MainPage as MasterDetailPage).Detail.Navigation.PushAsync(page);
        }

        public static async Task PushModalAsync(Page page)
        {
            await (Application.Current.MainPage as MasterDetailPage).Detail.Navigation.PushModalAsync(page, true);
        }

        public static void ChangeDetail(Page page, bool isNavigation = true)
        {
            var master = (Application.Current.MainPage as MasterDetailPage);

            if (isNavigation)
            {
                master.Detail = new NavigationPage(page)
                {
                    BarBackgroundColor = (Color)Application.Current.Resources[Constants.Resources.Colors.BarBackgroundColor]
                };
            }
            else
            {
                master.Detail = page;
            }
        }
    }
}
