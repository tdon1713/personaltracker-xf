﻿namespace PersonalTrackerXF.Helper
{
    public static partial class ChartHelper
    {
        public static class CHColors
        {
            public static readonly string Red = "#e54242";
            public static readonly string Orange = "#ff0000";
            public static readonly string Yellow = "#ffaa00";
            public static readonly string YellowGreen = "#ffff00";
            public static readonly string Green = "#95ff00";
        }
    }
}
