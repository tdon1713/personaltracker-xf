﻿using Microcharts;
using PersonalTrackerXF.Models;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PersonalTrackerXF.Helper
{
    public static partial class ChartHelper
    {
        public const int NumDaysToShow = 7;

        public static List<Entry> GetBookEntries(List<string> labels, List<Book> books, Func<Book, string, bool> countExp)
        {
            List<Entry> entries = new List<Entry>();

            foreach (var label in labels)
            {
                int value = books.Count(item => countExp(item, label));

                string color;
                if (value == 0)
                    color = CHColors.Red;
                else if (value <= 2)
                    color = CHColors.Orange;
                else if (value <= 4)
                    color = CHColors.Yellow;
                else if (value <= 6)
                    color = CHColors.YellowGreen;
                else 
                    color = CHColors.Green;

                entries.Add(new Entry(value)
                {
                    Label = label,
                    ValueLabel = value.ToString(),
                    Color = SKColor.Parse(color),
                    TextColor = SKColors.WhiteSmoke
                });
            }

            return entries;
        }

        public static List<string> GetDayLabelsForChart(DateTime startDate, int numDays)
        {
            List<string> labels = new List<string>();
            for (int a = 0; a < numDays; a++)
            {
                labels.Add(startDate.AddDays(a * -1).ToString(Formats.DateLabel));
            }

            labels.Reverse();
            return labels;
        }
    }
}
