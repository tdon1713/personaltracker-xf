﻿using System;
using Xamarin.Forms;

namespace PersonalTrackerXF.Helper
{
    public static class Utility
    {
        public static void OpenUrl(string url)
        {
            Device.OpenUri(new Uri(url));
        }

        public static Color GetStatusColor(OrderStatus status)
        {
            switch (status)
            {
                case OrderStatus.Preordered:
                    return Color.GhostWhite;
                case OrderStatus.Ordered:
                    return Color.LightBlue;
                case OrderStatus.In_Transit:
                    return Color.MediumPurple;
                case OrderStatus.Delivered:
                    return Color.LightGreen;
                case OrderStatus.Cancelled:
                    return Color.LightCoral;
            }

            return Color.White;
        }
    }
}
