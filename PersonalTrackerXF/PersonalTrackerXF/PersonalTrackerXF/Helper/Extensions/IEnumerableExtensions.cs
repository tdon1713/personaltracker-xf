﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace PersonalTrackerXF.Helper
{
    public static class IEnumerableExtensions 
    {
        public static ObservableCollection<T> ToObsCollection<T>(this IEnumerable<T> source)
        {
            return new ObservableCollection<T>(source);
        }
    }
}
