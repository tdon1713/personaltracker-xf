﻿using System;
using System.Linq;
using System.Reflection;

namespace PersonalTrackerXF.Helper
{
    public static class ObjectExtensions
    {
        public static T Copy<T>(this T source)
        {
            var copy = (T)Activator.CreateInstance(typeof(T));
            var propInfo = source.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly);
            foreach (var item in propInfo)
            {
                if (item.CanWrite)
                {
                    typeof(T).GetProperty(item.Name).SetValue(copy, item.GetValue(source, null), null);
                }
            }

            return copy;
        }

        public static bool In<T>(this T source, params T[] values)
        {
            return values.Contains(source);
        }
    }
}
