﻿using PersonalTrackerXF.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PersonalTrackerXF.Helper
{
    public static class EnumExtensions
    {
        public static string ToUIString<T>(this T sourceEnum) where T : struct
        {
            return sourceEnum.ToString().Replace('_', ' ');
        }

        public static IEnumerable<EnumerationViewModel> ListFrom<T>()
        {
            var array = Enum.GetValues(typeof(T)).Cast<T>();
            return array.Select(item => new EnumerationViewModel
            {
                Text = item.ToString().Replace("_", " "),
                Value = Convert.ToInt32(item)
            }).OrderBy(item => item.Text);
        }

        public static T ToEnum<T>(this EnumerationViewModel enumeration) where T : struct
        {
            return Enum.GetValues(typeof(T)).Cast<T>().FirstOrDefault(item => Convert.ToInt32(item) == enumeration.Value);
        }
    }
}
