﻿namespace PersonalTrackerXF.Helper
{
    public static partial class Constants
    {
        public static class Resources
        {
            public static class Colors
            {
                public static readonly string BarBackgroundColor = "MediumBlue";
            }

            public static class FontAwesome
            {
                public static readonly string Book = "\uf02d";
                public static readonly string Cog = "\uf013";
                public static readonly string Dashboard = "\uf3fd";
                public static readonly string Minus = "\uf068";
                public static readonly string Plus = "\uf067";
                public static readonly string StoreAlt = "\uf54f";
                public static readonly string Users = "\uf0c0";
            }
        }
    }
}
