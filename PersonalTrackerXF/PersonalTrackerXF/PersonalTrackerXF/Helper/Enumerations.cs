﻿namespace PersonalTrackerXF.Helper
{
    public enum ReadStatus
    {
        Reading,
        On_Hold,
        Plan_To_Read,
        Completed,
        Dropped
    }

    public enum VideoType
    {
        Movie,
        TV_Show,
    }

    public enum OrderStatus
    {
        In_Transit,
        Ordered,
        Preordered,
        Delivered,
        Cancelled
    }

    public enum BookGroupType
    {
        Status,
        Author
    }

    public enum MenuItem
    {
        Overview,
        Authors,
        Books,
        Orders,
        Settings,
        NewBook,
        NewAuthor,
        NewOrder
    }

    public enum BookType
    {
        Not_Available,
        Comic,
        Light_Novel,
        Manga,
        Novel
    }

    public enum Subfolders
    {
        Covers_Books,
        Covers_Books_Temp
    }

    public enum SupportedFileType
    {
        PNG,
        JPEG
    }

    public enum BookOwnedType
    {
        Physical,
        Kindle, 
        PDF, 
    }
}
