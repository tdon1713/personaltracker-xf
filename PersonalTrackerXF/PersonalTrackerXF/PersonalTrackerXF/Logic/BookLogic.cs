﻿using PersonalTrackerXF.Persistence;
using PersonalTrackerXF.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using System.Linq;
using PersonalTrackerXF.Helper;
using PersonalTrackerXF.ViewModels;
using Xamarin.Forms;
using PersonalTrackerXF.ServiceInterfaces;
using Microcharts;
using SkiaSharp;

namespace PersonalTrackerXF.Logic
{
    public static class BookLogic
    {
        private static readonly BookPersistence _dataAccess = new BookPersistence();

        public static async Task DeleteAsync(Author author)
        {
            await _dataAccess.DeleteAsync(author);
        }

        public static async Task DeleteAsync(Book book)
        {
            await _dataAccess.DeleteAsync(book);
        }

        public static async Task<List<Book>> Get()
        {
            return await _dataAccess.GetAsync();
        }

        public static async Task<Book> Get(int id)
        {
            Book book = await _dataAccess.GetAsync(id);

            return book;
        }

        public static async Task<List<Book>> GetForAuthor(int id)
        {
            return await _dataAccess.GetForAuthorAsync(id);
        }

        public static async Task<IEnumerable<IGrouping<ReadStatus, Book>>> Get(Func<Book, ReadStatus> groupBy)
        {
            var books = await Get();
            return books.GroupBy(groupBy);
        }

        public static async Task<List<Book>> GetRecentlyCreated(DateTime startDate)
        {
            return (await Get()).Where(item => item.CreatedDate.Date >= startDate.AddDays(ChartHelper.NumDaysToShow * -1)).ToList();
        }

        public static async Task<Chart> GetRecentlyCreatedChart()
        {
            var startDate = DateTime.Now.Date;
            var books = await GetRecentlyCreated(startDate);

            var labels = ChartHelper.GetDayLabelsForChart(startDate, ChartHelper.NumDaysToShow);
            var entries = ChartHelper.GetBookEntries(labels, books, (item, label) => item.CreatedDate.Date.ToString(ChartHelper.Formats.DateLabel) == label);

            return new LineChart()
            {
                Entries = entries,
                LineMode = LineMode.Spline,
                LineSize = 2,
                LabelTextSize = 12,
                PointMode = PointMode.Circle,
                PointSize = 10,
                BackgroundColor = SKColors.Transparent
            };
        }

        public static async Task<List<Book>> GetRecentlyCompleted(DateTime startDate)
        {
            return (await Get()).Where(item => item.Status == ReadStatus.Completed && item.DateCompleted.Value.Date >= startDate.AddDays(ChartHelper.NumDaysToShow * -1)).ToList();
        }

        public static async Task<Chart> GetRecentlyCompletedChart()
        {
            var startDate = DateTime.Now.Date;
            var books = await GetRecentlyCompleted(startDate);

            var labels = ChartHelper.GetDayLabelsForChart(startDate, ChartHelper.NumDaysToShow);
            var entries = ChartHelper.GetBookEntries(labels, books, (item, label) => item.DateCompleted?.Date.ToString(ChartHelper.Formats.DateLabel) == label);

            return new LineChart()
            {
                Entries = entries,
                LineMode = LineMode.Spline,
                LineSize = 2,
                LabelTextSize = 12,
                PointMode = PointMode.Circle,
                PointSize = 10,
                BackgroundColor = SKColors.Transparent
            };
        }

        public static async Task<BookItemViewModel> Save(BookItemViewModel bookItem)
        {
            Book book = bookItem.SourceObject ?? new Book();
            book.Title = bookItem.Title;
            book.Volume = bookItem.Volume;
            book.Chapters = bookItem.Chapters;
            book.PageCount = bookItem.Pages;
            book.Status = bookItem.Status.ToEnum<ReadStatus>();
            book.Type = bookItem.Type.ToEnum<BookType>();
            book.OwnedType = bookItem.OwnedType.ToEnum<BookOwnedType>();
            book.DateStarted = bookItem.DateStarted;
            book.DateCompleted = bookItem.DateCompleted;
            book.Notes = bookItem.Notes;
            book.AuthorID = bookItem.Author?.ID ?? -1;

            if (!string.IsNullOrWhiteSpace(bookItem.DownloadLocation) && !string.IsNullOrEmpty(bookItem.CoverUrl))
            {
                var fileHelper = DependencyService.Get<IFileHelper>();
                string newLocation = await fileHelper.Move(bookItem.DownloadLocation, Subfolders.Covers_Books);
                book.DownloadLocation = newLocation;
            }

            if (book.Status != ReadStatus.Completed)
            {
                book.DateCompleted = DateTime.MinValue;
            }

            bookItem.SourceObject = await _dataAccess.SaveAsync(book);
            return bookItem;
        }
    }
}
