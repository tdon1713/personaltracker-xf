﻿using PersonalTrackerXF.Helper;
using PersonalTrackerXF.Models;
using PersonalTrackerXF.Persistence;
using PersonalTrackerXF.ViewModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonalTrackerXF.Logic
{
    public static class OrderLogic
    {
        private static readonly OrderPersistence _dataAccess = new OrderPersistence();
        private static readonly OrderItemPersistence _itemDataAccess = new OrderItemPersistence();

        public static async Task Delete(Order order)
        {
            await _itemDataAccess.DeleteAsync(order);
            await _dataAccess.DeleteAsync(order);
        }

        public static async Task Delete(OrderItem item)
        {
            await _itemDataAccess.DeleteAsync(item);
        }

        public static async Task<List<Order>> Get()
        {
            return await _dataAccess.GetAsync();
        }
        
        public static async Task<Order> Get(int id)
        {
            return await _dataAccess.GetAsync(id);
        }

        public static async Task<IEnumerable<IGrouping<OrderStatus, Order>>> Get(Func<Order, OrderStatus> groupBy)
        {
            var orders = await Get();
            return orders.GroupBy(groupBy).OrderBy(item => item.Key);
        }

        public static async Task<IEnumerable<OrderItem>> GetItems(int orderId)
        {
            return await _itemDataAccess.GetItemsAsync(orderId);
        }

        public static async Task<List<Order>> GetToday()
        {
            return (await Get()).Where(item => item.Status != OrderStatus.Cancelled && item.ArrivalDate.Date == DateTime.Now.Date).ToList();
        }

        public static async Task<List<Order>> GetUpcoming()
        {
            return (await Get()).Where(item => item.Status != OrderStatus.Cancelled && item.ArrivalDate.Date > DateTime.Now.Date)
                .Take(Globals.Instance.Settings.UpcomingOrdersCountOnHome)
                .ToList();
        }

        public static async Task<OrderItemViewModel> Save(OrderItemViewModel order)
        {
            Order newOrder = order.SourceObject ?? new Order();
            newOrder.Name = order.Name;
            newOrder.Status = order.Status.ToEnum<OrderStatus>();
            newOrder.TrackingNumber = order.TrackingNumber;
            newOrder.Url = order.Url;
            newOrder.Price = order.Price;
            newOrder.PurchaseDate = order.PurchaseDate.Date;
            newOrder.ArrivalDate = order.ArrivalDate.Date;

            order.SourceObject = await _dataAccess.SaveAsync(newOrder);
            return order;
        }

        public static async Task<OrderItemListItemViewModel> Save(OrderItemListItemViewModel orderItem, int orderId)
        {
            OrderItem newItem = orderItem.SourceObject ?? new OrderItem();
            newItem.OrderID = orderId;
            newItem.Description = orderItem.Description;
            newItem.Url = orderItem.Url;
            newItem.Price = orderItem.Price;

            orderItem.SourceObject = await _itemDataAccess.SaveAsync(newItem);
            return orderItem;
        }
    }
}
