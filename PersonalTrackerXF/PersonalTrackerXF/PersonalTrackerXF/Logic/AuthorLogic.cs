﻿using PersonalTrackerXF.Helper;
using PersonalTrackerXF.Models;
using PersonalTrackerXF.Persistence;
using PersonalTrackerXF.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PersonalTrackerXF.Logic
{
    public static class AuthorLogic
    {
        private static readonly AuthorPersistence _dataAccess = new AuthorPersistence();

        public static async Task DeleteAsync(int id)
        {
            await _dataAccess.DeleteAsync(id);
        }

        public static async Task DeleteAsync(Author author)
        {
            await _dataAccess.DeleteAsync(author);
            await BookLogic.DeleteAsync(author);
        }

        public static async Task<List<Author>> GetAsync()
        {
            return await _dataAccess.GetAsync();
        }

        public static async Task<Author> GetAsync(int id)
        {
            return await _dataAccess.GetAsync(id);
        }

        public static async Task<AuthorItemViewModel> Save(AuthorItemViewModel author)
        {
            if (string.IsNullOrEmpty(author.FirstName))
                throw new ArgumentException(AppResource.Error_InvalidLength, nameof(author.FirstName));

            if (string.IsNullOrEmpty(author.LastName))
                throw new ArgumentException(AppResource.Error_InvalidLength, nameof(author.LastName));

            var newAuthor = author.SourceObject ?? new Author();
            newAuthor.FirstName = author.FirstName;
            newAuthor.LastName = author.LastName;

            author.SourceObject = await _dataAccess.SaveAsync(newAuthor);
            return author;
        }

        public static async Task UpdateStats(IEnumerable<Author> authors)
        {
            foreach (var author in authors)
            {
                var books  = await BookLogic.GetForAuthor(author.ID);
                author.BookCount = books.Count;

                await _dataAccess.SaveAsync(author);
            }
        }
    }
}
