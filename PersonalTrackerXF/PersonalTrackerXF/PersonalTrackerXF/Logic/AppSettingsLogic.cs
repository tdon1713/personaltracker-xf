﻿using PersonalTrackerXF.Models;
using PersonalTrackerXF.Persistence;
using PersonalTrackerXF.ViewModels;
using System.Threading.Tasks;

namespace PersonalTrackerXF.Logic
{
    public static class AppSettingsLogic
    {
        #region Private Variables

        private static readonly AppSettingsPersistence _dataAccess = new AppSettingsPersistence();

        #endregion

        #region Public Methods

        public static async Task<AppSettings> Insert(AppSettings settings = null)
        {
            return await _dataAccess.SaveAsync(settings ?? new AppSettings());
        }

        public static async Task<AppSettings> Get()
        {
            return await _dataAccess.GetAsync(1);
        }

        public static async Task<AppSettings> Update(SettingsViewModel settings)
        {
            settings.SourceObject.ShowOrderPriceOnHome = settings.ShowOrderPriceOnHome;
            settings.SourceObject.UpcomingOrdersCountOnHome = settings.UpcomingOrdersCountOnHome;
            return await _dataAccess.SaveAsync(settings.SourceObject);
        }

        #endregion
    }
}
