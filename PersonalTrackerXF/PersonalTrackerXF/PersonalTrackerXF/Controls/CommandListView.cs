﻿using System.Windows.Input;
using Xamarin.Forms;

namespace PersonalTrackerXF.Controls
{
    public class CommandListView : ListView
    {
        public static readonly BindableProperty ItemSelectedCommandProperty = BindableProperty.Create(
            nameof(ItemSelectedCommand),
            typeof(ICommand),
            typeof(CommandListView),
            defaultValue: null);

        public ICommand ItemSelectedCommand
        {
            get => (ICommand)GetValue(ItemSelectedCommandProperty);
            set => SetValue(ItemSelectedCommandProperty, value);
        }

        public CommandListView()
        {
            ItemTapped += Handle_ItemTapped;
        }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();

            if (BindingContext == null)
                ItemTapped -= Handle_ItemTapped;
        }

        private void Handle_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item != null && ItemSelectedCommand != null && ItemSelectedCommand.CanExecute(e.Item))
            {
                ItemSelectedCommand.Execute(e.Item);
                SelectedItem = null;
            }
        }
    }
}
