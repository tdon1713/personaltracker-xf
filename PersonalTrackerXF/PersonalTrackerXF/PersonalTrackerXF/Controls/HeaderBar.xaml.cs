﻿using PersonalTrackerXF.Helper;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PersonalTrackerXF.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HeaderBar : ContentView
	{
        public static readonly BindableProperty IsModalProperty = BindableProperty.Create(
            nameof(IsModal),
            typeof(bool),
            typeof(HeaderBar),
            defaultValue: false);

        public bool IsModal
        {
            get => (bool)GetValue(IsModalProperty);
            set => SetValue(IsModalProperty, value);
        }

        public static readonly BindableProperty SaveCommandProperty = BindableProperty.Create(
            nameof(SaveCommand),
            typeof(ICommand),
            typeof(HeaderBar),
            defaultValue: null,
            propertyChanged: (sender, oldValue, newValue) =>
            {
                var control = (HeaderBar)sender;
                control.SaveButton.IsVisible = newValue != null;
            });

        public ICommand SaveCommand
        {
            get => (ICommand)GetValue(SaveCommandProperty);
            set => SetValue(SaveCommandProperty, value);
        }

        public static readonly BindableProperty DeleteCommandProperty = BindableProperty.Create(
           nameof(DeleteCommand),
           typeof(ICommand),
           typeof(HeaderBar),
           defaultValue: null,
           propertyChanged: (sender, oldValue, newValue) =>
           {
               var control = (HeaderBar)sender;
               control.DeleteButton.IsVisible = newValue != null;
           });

        public ICommand DeleteCommand
        {
            get => (ICommand)GetValue(DeleteCommandProperty);
            set => SetValue(DeleteCommandProperty, value);
        }

        public HeaderBar ()
		{
			InitializeComponent ();
		}

        private async void CloseButton_Clicked(object sender, System.EventArgs e)
        {
            if (IsModal)
                await Paging.PopModalAsync();

            await Paging.PopDetailAsync();
        }

        private void SaveButton_Clicked(object sender, System.EventArgs e)
        {
            if (SaveCommand != null && SaveCommand.CanExecute(null))
            {
                SaveCommand.Execute(null);
            }
        }
        
        private void DeleteButton_Clicked(object sender, System.EventArgs e)
        {
            if (DeleteCommand != null && DeleteCommand.CanExecute(null))
            {
                DeleteCommand.Execute(null);
            }
        }
    }
}