﻿
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PersonalTrackerXF.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class GridItem : ContentView
	{
        public static readonly BindableProperty InputLayoutProperty = BindableProperty.Create(
            nameof(InputLayout),
            typeof(ContentView),
            typeof(GridItem),
            defaultValue: null,
            propertyChanged: (sender, oldValue, newValue) =>
            {
                var control = (GridItem)sender;
                if (newValue != null)
                {
                    control.container.Children.Add((ContentView)newValue);
                }
            });

        public ContentView InputLayout
        {
            get => (ContentView)GetValue(InputLayoutProperty);
            set => SetValue(InputLayoutProperty, value);
        }

        public static readonly BindableProperty TextProperty = BindableProperty.Create(
            nameof(Text),
            typeof(string),
            typeof(GridItem),
            defaultValue: null,
            propertyChanged: (sender, oldValue, newValue) =>
            {
                var control = (GridItem)sender;
                if (newValue != null)
                {
                    control.lblText.Text = (string)newValue;
                    control.lblText.IsVisible = true;
                }
                else
                {
                    control.lblText.IsVisible = false;
                }
            });

        public string Text
        {
            get => (string)GetValue(TextProperty);
            set => SetValue(TextProperty, value);
        }

        public static readonly BindableProperty ResetCommandProperty = BindableProperty.Create(
            nameof(ResetCommand),
            typeof(ICommand),
            typeof(GridItem),
            defaultValue: null,
            propertyChanged: (sender, oldValue, newValue) =>
            {
                var control = (GridItem)sender;
                if (sender != null)
                {
                    control.lblReset.IsVisible = true;
                    control.lblReset.GestureRecognizers.Clear();
                    control.lblReset.GestureRecognizers.Add(new TapGestureRecognizer
                    {
                        Command = (ICommand)newValue
                    });
                }
                else
                {
                    control.lblReset.IsVisible = false;
                }
            });

        public ICommand ResetCommand
        {
            get => (ICommand)GetValue(ResetCommandProperty);
            set => SetValue(ResetCommandProperty, value);
        }

		public GridItem ()
		{
			InitializeComponent ();
		}
	}
}