﻿using SQLite;

namespace PersonalTrackerXF.Models
{
    [Table("Authors")]
    public class Author : BaseModel
    {
        public Author() { }
        
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int BookCount { get; set; } = 0;

        [Ignore]
        public string Name => $"{LastName}, {FirstName}";

        public override string ToString()
        {
            return Name;
        }
    }
}
