﻿using PersonalTrackerXF.Helper;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PersonalTrackerXF.Models
{
    [Table("Orders")]
    public class Order : BaseModel
    {
        public Order() { }

        public Order(Order order)
        {
            ID = order.ID;
            Name = order.Name;
            Status = order.Status;
            TrackingNumber = order.TrackingNumber;
            Url = order.Url;
            Price = order.Price;
            PurchaseDate = order.PurchaseDate;
            ArrivalDate = order.ArrivalDate;
        }

        public Order(Order order, IEnumerable<OrderItem> items) : this(order)
        {
            Items = (items ?? new List<OrderItem>()).ToList();
        }
        
        public string Name { get; set; }

        public OrderStatus Status { get; set; } = OrderStatus.Ordered;

        public string TrackingNumber { get; set; }

        public string Url { get; set; }

        public double Price { get; set; }

        public DateTime PurchaseDate { get; set; }

        public DateTime ArrivalDate { get; set; }

        [Ignore]
        public List<OrderItem> Items { get; set; }

        [Ignore]
        public int ItemCount => Items?.Count ?? 0;
    }
}
