﻿using PersonalTrackerXF.Helper;
using SQLite;
using System;

namespace PersonalTrackerXF.Models
{
    [Table("Videos")]
    public class Video : BaseModel
    {
        public Video() { }

        public string Name { get; set; }

        public int? Season { get; set; }
       
        public int? Episode { get; set; }

        public int LengthInMinutes { get; set; }

        public DateTime? AirDate { get; set; } = null;

        public DateTime? WatchedDate { get; set; } = null;

        public VideoType Type { get; set; } = VideoType.TV_Show;
    }
}
