﻿namespace PersonalTrackerXF.Models
{
    public class AppSettings : BaseModel
    {
        public int UpcomingOrdersCountOnHome { get; set; } = 5;

        public bool ShowOrderPriceOnHome { get; set; } = false;
    }
}
