﻿using SQLite;

namespace PersonalTrackerXF.Models
{
    [Table("OrderItems")]
    public class OrderItem : BaseModel
    {
        public OrderItem() { }

        [NotNull]
        public int OrderID { get; set; }

        [NotNull]
        public string Description { get; set; }

        public string Url { get; set; }

        public double Price { get; set; }
    }
}
