﻿using SQLite;
using System;

namespace PersonalTrackerXF.Models
{
    public class BaseModel
    {
        [PrimaryKey, AutoIncrement]
        public virtual int ID { get; set; }

        public bool Active { get; set; } = true;

        public DateTime CreatedDate { get; set; } = DateTime.Now;
    }
}
