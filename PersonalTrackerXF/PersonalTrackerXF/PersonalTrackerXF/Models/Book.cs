﻿using PersonalTrackerXF.Helper;
using SQLite;
using System;

namespace PersonalTrackerXF.Models
{
    [Table("Books")]
    public class Book : BaseModel
    {
        public Book() { }

        public Book(Book book, Author author)
        {
            ID = book.ID;
            Title = book.Title;
            PageCount = book.PageCount;
            Volume = book.Volume;
            Chapters = book.Chapters;
            DateStarted = book.DateStarted;
            DateCompleted = book.DateCompleted;
            Status = book.Status;
            Type = book.Type;
            CoverUrl = book.CoverUrl;
            DownloadLocation = book.DownloadLocation;
            Notes = book.Notes;
            AuthorID = book.AuthorID;
            Active = book.Active;
            CreatedDate = book.CreatedDate;
            Author = author;
            OwnedType = book.OwnedType;
        }

        public string Title { get; set; }

        public int AuthorID { get; set; }

        [Ignore]
        public Author Author { get; set; }

        public int PageCount { get; set; }

        public int Volume { get; set; }

        public int Chapters { get; set; }

        public DateTime? DateStarted { get; set; } = null;

        public DateTime? DateCompleted { get; set; } = null;

        public ReadStatus Status { get; set; } = ReadStatus.Plan_To_Read;

        public BookType Type { get; set; }

        public BookOwnedType OwnedType { get; set; } = BookOwnedType.Physical;

        public string CoverUrl { get; set; }

        public string DownloadLocation { get; set; }
        
        public string Notes { get; set; }
    }
}
