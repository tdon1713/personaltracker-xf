﻿using PersonalTrackerXF.Helper;
using PersonalTrackerXF.Logic;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace PersonalTrackerXF.ViewModels
{
    public class SettingsPageViewModel : BasePageViewModel
    {
        #region Private Variables

        private SettingsViewModel _settings;

        #endregion

        #region Public Properties

        public SettingsViewModel Settings
        {
            get => _settings;
            set => SetValue(ref _settings, value);
        }

        #endregion

        #region Command

        private ICommand _saveCommand;
        public ICommand SaveCommand => _saveCommand ?? (_saveCommand = new Command(() => Save()));

        #endregion

        #region Constructor

        public SettingsPageViewModel() { }

        #endregion

        #region Override Methods

        public override async Task InitAsync()
        {
            await base.InitAsync();
            Settings = new SettingsViewModel(Globals.Instance.Settings);
        }

        #endregion

        #region Private Methods

        private async void Save()
        {
            ToggleLoading();

            _settings.SourceObject = await AppSettingsLogic.Update(_settings);
            Globals.Instance.Settings = _settings.SourceObject;

            ToggleLoading();
        }

        #endregion
    }
}
