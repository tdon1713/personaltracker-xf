﻿using PersonalTrackerXF.Models;

namespace PersonalTrackerXF.ViewModels
{
    public class SettingsViewModel : BaseViewModel<AppSettings>
    {
        private int _upcomingOrdersCountOnHome;
        private bool _showOrderPriceOnHome;

        public SettingsViewModel(AppSettings settings) : base(settings)
        {
            UpcomingOrdersCountOnHome = settings.UpcomingOrdersCountOnHome;
            ShowOrderPriceOnHome = settings.ShowOrderPriceOnHome;
        }

        public int UpcomingOrdersCountOnHome
        {
            get => _upcomingOrdersCountOnHome;
            set => SetValue(ref _upcomingOrdersCountOnHome, value);
        }

        public bool ShowOrderPriceOnHome
        {
            get => _showOrderPriceOnHome;
            set => SetValue(ref _showOrderPriceOnHome, value);
        }
    }
}
