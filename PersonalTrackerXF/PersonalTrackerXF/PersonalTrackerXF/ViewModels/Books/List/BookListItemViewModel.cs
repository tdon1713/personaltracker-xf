﻿using PersonalTrackerXF.Helper;
using PersonalTrackerXF.Models;

namespace PersonalTrackerXF.ViewModels
{
    public class BookListItemViewModel : BaseViewModel<Book>
    {
        public BookListItemViewModel(Book book) : base(book)
        {
            BookTitle = book.Title;
            AuthorName = book.Author?.Name;
            Type = book.Type == BookType.Not_Available ? string.Empty : book.Type.ToUIString();
            OwnedType = book.OwnedType.ToString();
            ImageUrl = book.DownloadLocation;

            if (book.Chapters > 0)
                Chapters = book.Chapters;

            if (book.Volume > 0)
                BookTitle += $", Vol. {book.Volume}";

            if (book.PageCount > 0)
                Pages = book.PageCount;
        }

        public string BookTitle { get; set; }

        public string AuthorName { get; set; }

        public string Type { get; set; }

        public int? Chapters { get; set; } = null;

        public int? Pages { get; set; } = null;

        public string ImageUrl { get; set; }

        public string OwnedType { get; set; }
    }
}
