﻿using PersonalTrackerXF.Helper;
using System.Collections.Generic;

namespace PersonalTrackerXF.ViewModels
{
    public class BookGroupListViewModel : List<BookListItemViewModel>
    {
        public BookGroupListViewModel(ReadStatus status)
        {
            Status = status;
        }

        public ReadStatus Status { get; set; }

        public string Header
        {
            get => Status.ToUIString();
        }

        public List<BookListItemViewModel> Books => this;
    }
}
