﻿using PersonalTrackerXF.Helper;
using PersonalTrackerXF.Logic;
using PersonalTrackerXF.Pages;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace PersonalTrackerXF.ViewModels
{
    public class BookListPageViewModel : BasePageViewModel
    {
        private ObservableCollection<BookGroupListViewModel> _books;
        public ObservableCollection<BookGroupListViewModel> Books
        {
            get => _books;
            set => SetValue(ref _books, value);
        }

        private string _searchText;
        public string SearchText
        {
            get => _searchText;
            set => SetValue(ref _searchText, value);
        }

        private ICommand _itemTappedCommand;
        public ICommand ItemTappedCommand => _itemTappedCommand ?? (_itemTappedCommand = new Command<BookListItemViewModel>((item) => ItemTapped(item)));

        private ICommand _searchCommand;
        public ICommand SearchCommand => _searchCommand ?? (_searchCommand = new Command(async () => await Search()));

        public BookListPageViewModel() { }

        public override async Task InitAsync()
        {
            await base.InitAsync();
            await Search();
        }

        private async void ItemTapped(BookListItemViewModel item)
        {
            if (item == null)
                return;

            await Paging.PushDetailAsync(new BookItemPage(item));
        }

        private async Task Search()
        {
            ToggleLoading();
            var groupedBooks = await BookLogic.Get(item => item.Status);

            var collection = new ObservableCollection<BookGroupListViewModel>();
            foreach (var status in groupedBooks)
            {
                var groupCollection = new BookGroupListViewModel(status.Key);
                foreach (var item in status)
                {
                    if (!string.IsNullOrEmpty(SearchText) && !item.Title.ToUpper().Contains(SearchText.ToUpper()))
                        continue;

                    groupCollection.Add(new BookListItemViewModel(item));
                }

                if (groupCollection.Count > 0)
                    collection.Add(groupCollection);
            }

            Title = collection.Sum(item => item.Count) + " Books";
            Books = collection;

            ToggleLoading();
        }
    }
}
