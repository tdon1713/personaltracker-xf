﻿using PersonalTrackerXF.Helper;
using PersonalTrackerXF.Models;
using System;

namespace PersonalTrackerXF.ViewModels
{
    public class BookItemViewModel : BaseViewModel<Book>
    {
        private string _title;
        private int _volume;
        private int _chapters;
        private int _pages;
        private EnumerationViewModel _status;
        private EnumerationViewModel _type;
        private EnumerationViewModel _ownedType;
        private DateTime _dateStarted = DateTime.Today;
        private DateTime _dateCompleted = DateTime.Today;
        private string _coverUrl;
        private string _downloadLocation;
        private string _notes;
        private Author _author;

        public BookItemViewModel(Book book) : base(book)
        {
            if (book == null)
                return;

            Title = book.Title;
            Volume = book.Volume;
            Chapters = book.Chapters;
            Pages = book.PageCount;
            DateStarted = book.DateStarted ?? DateTime.Today;
            DateCompleted = book.DateCompleted ?? DateTime.Today;
            DownloadLocation = book.DownloadLocation;
            Notes = book.Notes;
        }

        public string Title
        {
            get => _title;
            set => SetValue(ref _title, value);
        }

        public Author Author
        {
            get => _author;
            set => SetValue(ref _author, value);
        }

        public int Volume
        {
            get => _volume;
            set => SetValue(ref _volume, value);
        }

        public int Chapters
        {
            get => _chapters;
            set => SetValue(ref _chapters, value);
        }

        public int Pages
        {
            get => _pages;
            set => SetValue(ref _pages, value);
        }

        public EnumerationViewModel Status
        {
            get => _status;
            set
            {
                SetValue(ref _status, value);
                OnPropertyChanged(nameof(IsStarted));
                OnPropertyChanged(nameof(IsCompleted));
            }
        }

        public EnumerationViewModel Type
        {
            get => _type;
            set => SetValue(ref _type, value);
        }

        public EnumerationViewModel OwnedType
        {
            get => _ownedType;
            set => SetValue(ref _ownedType, value);
        }

        public DateTime DateStarted
        {
            get => _dateStarted;
            set => SetValue(ref _dateStarted, value);
        }

        public DateTime DateCompleted
        {
            get => _dateCompleted;
            set => SetValue(ref _dateCompleted, value);
        }

        public string CoverUrl
        {
            get => _coverUrl;
            set => SetValue(ref _coverUrl, value);
        }

        public string DownloadLocation
        {
            get => _downloadLocation;
            set => SetValue(ref _downloadLocation, value);
        }

        public string Notes
        {
            get => _notes;
            set => SetValue(ref _notes, value);
        }

        public bool IsCompleted => ((ReadStatus)Status.Value).In(ReadStatus.Completed, ReadStatus.Dropped);

        public bool IsStarted => ((ReadStatus)Status.Value).In(ReadStatus.Reading, ReadStatus.On_Hold, ReadStatus.Dropped, ReadStatus.Completed);
    }
}
