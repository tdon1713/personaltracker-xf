﻿using PersonalTrackerXF.Helper;
using PersonalTrackerXF.Logic;
using PersonalTrackerXF.Models;
using PersonalTrackerXF.ServiceInterfaces;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace PersonalTrackerXF.ViewModels
{
    public class BookItemPageViewModel : BasePageViewModel
    {
        private BookListItemViewModel _selectedBook;

        private BookItemViewModel _item;
        public BookItemViewModel Item
        {
            get => _item;
            set => SetValue(ref _item, value);
        }

        private ObservableCollection<EnumerationViewModel> _statuses;
        public ObservableCollection<EnumerationViewModel> Statuses
        {
            get => _statuses ?? (_statuses = new ObservableCollection<EnumerationViewModel>(EnumExtensions.ListFrom<ReadStatus>()));
        }

        private ObservableCollection<EnumerationViewModel> _types;
        public ObservableCollection<EnumerationViewModel> Types
        {
            get => _types ?? (_types = new ObservableCollection<EnumerationViewModel>(EnumExtensions.ListFrom<BookType>().Where(item => item.Value != (int)BookType.Not_Available)));
        }

        private ObservableCollection<EnumerationViewModel> _ownedTypes;
        public ObservableCollection<EnumerationViewModel> OwnedTypes
        {
            get => _ownedTypes ?? (_ownedTypes = new ObservableCollection<EnumerationViewModel>(EnumExtensions.ListFrom<BookOwnedType>()));
        }

        private ObservableCollection<Author> _authors;
        public ObservableCollection<Author> Authors
        {
            get => _authors;
            set => SetValue(ref _authors, value);
        }

        private ICommand _saveBook;
        public ICommand SaveBook => _saveBook ?? (_saveBook = new Command(() => Save()));

        private ICommand _deleteBook;
        public ICommand DeleteBook => _deleteBook ?? (_deleteBook = new Command(() => Delete()));

        private ICommand _previewCoverCommand;
        public ICommand PreviewCoverCommand => _previewCoverCommand ?? (_previewCoverCommand = new Command(() => PreviewCover()));

        private ICommand _resetDateStartedCommand;
        public ICommand ResetDateStartedCommand => _resetDateStartedCommand ?? (_resetDateStartedCommand = new Command(() => Item.DateStarted = DateTime.Now.Date));

        private ICommand _resetDateCompletedCommand;
        public ICommand ResetDateCompletedCommand => _resetDateCompletedCommand ?? (_resetDateCompletedCommand = new Command(() => Item.DateCompleted = DateTime.Now.Date));

        public BookItemPageViewModel(BookListItemViewModel book)
        {
            _selectedBook = book;
        }

        public override async Task InitAsync()
        {
            await base.InitAsync();

            ToggleLoading();

            var authors = await AuthorLogic.GetAsync();
            Authors = new ObservableCollection<Author>(authors.OrderBy(item => item.Name));

            if (_selectedBook == null)
            {
                Item = new BookItemViewModel(null);
                Item.Status = Statuses.FirstOrDefault(item => item.Value == (int)ReadStatus.Plan_To_Read);
                Item.OwnedType = OwnedTypes.FirstOrDefault(item => item.Value == (int)BookOwnedType.Physical);
            }
            else
            {
                var book = await BookLogic.Get(_selectedBook.SourceObject.ID);
                Item = new BookItemViewModel(book);
                Item.Status = Statuses.FirstOrDefault(item => item.Value == (int)book.Status);
                Item.Author = Authors.FirstOrDefault(item => item.ID == book.AuthorID);
                Item.Type = Types.FirstOrDefault(item => item.Value == (int)book.Type);
                Item.OwnedType = OwnedTypes.FirstOrDefault(item => item.Value == (int)book.OwnedType);
            }

            ToggleLoading();
        }

        private async void Delete()
        {
            if (Item.SourceObject == null)
                return;

            bool delete = await ShowConfirm(AppResource.ConfirmHeaderAreYouSure, string.Format(AppResource.ConfirmDeleteBook, Item.SourceObject.Title));
            if (!delete)
                return;

            ToggleLoading();

            await BookLogic.DeleteAsync(Item.SourceObject);
            await Paging.PopDetailAsync();

            ToggleLoading();
        }

        private async void PreviewCover()
        {
            if (string.IsNullOrEmpty(Item.CoverUrl))
                return;

            ToggleLoading();
            
            var fileHelper = DependencyService.Get<IFileHelper>();
            (bool success, string filePath) = await fileHelper.Download(Item.CoverUrl, Subfolders.Covers_Books_Temp, SupportedFileType.PNG);
            if (success)
            {
                Item.DownloadLocation = filePath;
            }

            ToggleLoading();
        }

        private async void Save()
        {
            ToggleLoading();            

            await BookLogic.Save(Item);
            await Paging.PopDetailAsync();

            ToggleLoading();
        }
    }
}
