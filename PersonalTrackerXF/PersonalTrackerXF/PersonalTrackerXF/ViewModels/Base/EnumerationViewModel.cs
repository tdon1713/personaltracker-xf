﻿using System;

namespace PersonalTrackerXF.ViewModels
{
    public class EnumerationViewModel
    {
        public string Text { get; set; }

        public int Value { get; set; }

        public override string ToString()
        {
            return Text;
        }
    }
}
