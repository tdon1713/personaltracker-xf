﻿using Acr.UserDialogs;
using PersonalTrackerXF.Helper;
using PersonalTrackerXF.Persistence;
using System.Threading.Tasks;

namespace PersonalTrackerXF.ViewModels
{
    public class BasePageViewModel : BaseViewModel
    {
        #region Private Variables

        private string _title;
        private bool _loadShowing = false;

        #endregion

        #region Virtual Methods

        public virtual async Task InitAsync()
        {
            BasePersistence.Init();
            await Globals.InitAsync();
        }

        #endregion

        public string Title
        {
            get => _title;
            set => SetValue(ref _title, value);
        }

        public void ToggleLoading(string loadingText = "")
        {
            if (_loadShowing)
                UserDialogs.Instance.HideLoading();
            else
                UserDialogs.Instance.ShowLoading(title: loadingText, maskType: MaskType.Gradient);

            _loadShowing = !_loadShowing;
        }

        public async Task ShowAlert(string text, bool isError = false)
        {
            await UserDialogs.Instance.AlertAsync(text, title: (isError ? AppResource.AlertHeaderError : AppResource.AlertHeaderMessage));
        }

        public async Task<bool> ShowConfirm(string header, string text)
        {
            return await UserDialogs.Instance.ConfirmAsync(text, title: header);
        }
    }
}
