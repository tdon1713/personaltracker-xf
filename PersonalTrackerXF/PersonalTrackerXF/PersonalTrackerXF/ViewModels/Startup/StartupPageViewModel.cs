﻿using System.Threading.Tasks;
using PersonalTrackerXF.Helper;
using PersonalTrackerXF.Pages;
using PersonalTrackerXF.ServiceInterfaces;
using Xamarin.Forms;

namespace PersonalTrackerXF.ViewModels
{
    public class StartupPageViewModel : BasePageViewModel
    {
        #region Private Variables

        private string _loadingText = AppResource.StatusLoading;

        #endregion

        #region Public Properties

        public string LoadingText
        {
            get { return _loadingText; }
            set { SetValue(ref _loadingText, value); }
        }

        #endregion

        #region Constructor

        public StartupPageViewModel() { }

        #endregion

        #region Public Methods

        public override async Task InitAsync()
        {
            await base.InitAsync();

            var fileHelper = DependencyService.Get<IFileHelper>();
            await fileHelper.ClearFolder(Subfolders.Covers_Books_Temp);

            await Task.Delay(1000);

            Paging.MoveTo(new MainMasterPage());
        }

        #endregion
    }
}
