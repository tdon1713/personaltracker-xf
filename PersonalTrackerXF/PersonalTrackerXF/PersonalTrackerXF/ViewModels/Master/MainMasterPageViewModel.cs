﻿using Acr.UserDialogs;
using PersonalTrackerXF.Helper;
using PersonalTrackerXF.Pages;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace PersonalTrackerXF.ViewModels
{
    public class MainMasterPageViewModel : BasePageViewModel
    {
        private ObservableCollection<MenuItemViewModel> _menuItems;
        public ObservableCollection<MenuItemViewModel> MenuItems
        {
            get => _menuItems;
            set => SetValue(ref _menuItems, value);
        }

        private ICommand _menuItemSelected;
        public ICommand MenuItemSelected => _menuItemSelected ?? (_menuItemSelected = new Command<MenuItemViewModel>((item) => ItemSelected(item)));

        public MainMasterPageViewModel() { }

        public override async Task InitAsync()
        {
            await base.InitAsync();
            MenuItems = BuildMenu();
        }

        private ObservableCollection<MenuItemViewModel> BuildMenu()
        {
            var collection = new ObservableCollection<MenuItemViewModel>();
            collection.Add(new MenuItemViewModel { Title = AppResource.TitleOverview, FAIcon = Constants.Resources.FontAwesome.Dashboard, Item = Helper.MenuItem.Overview });
            collection.Add(new MenuItemViewModel { Title = AppResource.TitleAuthors, FAIcon = Constants.Resources.FontAwesome.Users, Item = Helper.MenuItem.Authors });
            collection.Add(new MenuItemViewModel { Title = AppResource.TitleBooks, FAIcon = Constants.Resources.FontAwesome.Book, Item = Helper.MenuItem.Books });
            collection.Add(new MenuItemViewModel { Title = AppResource.TitleOrders, FAIcon = Constants.Resources.FontAwesome.StoreAlt, Item = Helper.MenuItem.Orders });
            collection.Add(new MenuItemViewModel { Title = AppResource.TitleSettings, FAIcon = Constants.Resources.FontAwesome.Cog, Item = Helper.MenuItem.Settings });

            return collection;
        }

        private async Task ItemSelected(MenuItemViewModel item)
        {
            if (item == null)
                return;

            RefreshMenuItems();

            int curPosition;
            ObservableCollection<MenuItemViewModel> newCollection = null;
            switch (item.Item)
            {
                case Helper.MenuItem.Overview:
                    newCollection = RefreshMenuItems();
                    Paging.ChangeDetail(new OverviewPage());
                    break;
                case Helper.MenuItem.Authors:
                    newCollection = RefreshMenuItems();
                    curPosition = newCollection.IndexOf(item);
                    newCollection.Insert(++curPosition, new MenuItemViewModel { Title = AppResource.TitleNewAuthor, FAIcon = Constants.Resources.FontAwesome.Plus, Item = Helper.MenuItem.NewAuthor, IsChild = true });
                    Paging.ChangeDetail(new AuthorListPage());
                    break;
                case Helper.MenuItem.Books:
                    newCollection = RefreshMenuItems();
                    curPosition = newCollection.IndexOf(item);
                    newCollection.Insert(++curPosition, new MenuItemViewModel { Title = AppResource.TitleNewBook, FAIcon = Constants.Resources.FontAwesome.Plus, Item = Helper.MenuItem.NewBook, IsChild = true });
                    Paging.ChangeDetail(new BookListPage());
                    break;
                case Helper.MenuItem.Orders:
                    newCollection = RefreshMenuItems();
                    curPosition = newCollection.IndexOf(item);
                    newCollection.Insert(++curPosition, new MenuItemViewModel { Title = AppResource.TitleNewOrder, FAIcon = Constants.Resources.FontAwesome.Plus, Item = Helper.MenuItem.NewOrder, IsChild = true });
                    Paging.ChangeDetail(new OrderListPage());
                    break;
                case Helper.MenuItem.Settings:
                    newCollection = RefreshMenuItems();
                    Paging.ChangeDetail(new SettingsPage(), false);
                    break;
                case Helper.MenuItem.NewBook:
                    await Paging.PushDetailAsync(new BookItemPage());
                    break;
                case Helper.MenuItem.NewAuthor:
                    await Paging.PushDetailAsync(new AuthorItemPage());
                    break;
                case Helper.MenuItem.NewOrder:
                    await Paging.PushDetailAsync(new OrderItemPage());
                    break;
            }

            if (newCollection != null)
                MenuItems = newCollection;
        }

        private ObservableCollection<MenuItemViewModel> RefreshMenuItems()
        {
            var newCollection = new ObservableCollection<MenuItemViewModel>(MenuItems.ToList());
            foreach (var child in MenuItems)
            {
                if (child.IsChild)
                {
                    newCollection.Remove(child);
                }
            }

            return newCollection;
        }
    }
}
