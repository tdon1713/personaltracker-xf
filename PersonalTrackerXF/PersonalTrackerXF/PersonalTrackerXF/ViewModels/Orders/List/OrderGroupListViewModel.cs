﻿using PersonalTrackerXF.Helper;
using System.Collections.Generic;
using Xamarin.Forms;

namespace PersonalTrackerXF.ViewModels
{
    public class OrderGroupListViewModel : List<OrderListItemViewModel>
    {
        public OrderGroupListViewModel(OrderStatus status)
        {
            Status = status;
        }

        public OrderStatus Status { get; set; }

        public Color StatusColor => Utility.GetStatusColor(Status);

        public string Header => $"{Status.ToUIString()} ({Orders.Count})";

        public List<OrderListItemViewModel> Orders => this;
    }
}
