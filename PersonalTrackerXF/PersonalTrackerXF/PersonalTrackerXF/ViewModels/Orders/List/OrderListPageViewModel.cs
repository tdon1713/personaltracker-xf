﻿using PersonalTrackerXF.Helper;
using PersonalTrackerXF.Logic;
using PersonalTrackerXF.Pages;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace PersonalTrackerXF.ViewModels
{
    public class OrderListPageViewModel : BasePageViewModel
    {
        private ObservableCollection<OrderGroupListViewModel> _orders;
        public ObservableCollection<OrderGroupListViewModel> Orders
        {
            get => _orders;
            set => SetValue(ref _orders, value);
        }

        private string _searchText;
        public string SearchText
        {
            get => _searchText;
            set => SetValue(ref _searchText, value);
        }

        private ICommand _itemTappedCommand;
        public ICommand ItemTappedCommand => _itemTappedCommand ?? (_itemTappedCommand = new Command<OrderListItemViewModel>((item) => ItemTapped(item)));

        private ICommand _searchCommand;
        public ICommand SearchCommand => _searchCommand ?? (_searchCommand = new Command(async () => await Search()));

        public OrderListPageViewModel() { }

        public override async Task InitAsync()
        {
            await base.InitAsync();
            await Search();
        }

        public async Task Search()
        {
            ToggleLoading();

            var groupedOrders = await OrderLogic.Get(item => item.Status);
            var collection = new ObservableCollection<OrderGroupListViewModel>();

            foreach (var status in groupedOrders)
            {
                var groupCollection = new OrderGroupListViewModel(status.Key);
                foreach (var order in status)
                {
                    if (!string.IsNullOrWhiteSpace(SearchText) && !order.Name.ToUpper().Contains(SearchText.ToUpper()))
                        continue;

                    groupCollection.Add(new OrderListItemViewModel(order));
                }

                if (groupCollection.Count > 0)
                    collection.Add(groupCollection);
            }

            Title = collection.Sum(item => item.Count) + " Orders";
            Orders = collection;

            ToggleLoading();
        }

        private async void ItemTapped(OrderListItemViewModel item)
        {
            if (item == null)
                return;

            await Paging.PushDetailAsync(new OrderItemPage(item));
        }
    }
}
