﻿using PersonalTrackerXF.Helper;
using PersonalTrackerXF.Models;
using System;
using Xamarin.Forms;

namespace PersonalTrackerXF.ViewModels
{
    public class OrderListItemViewModel : BaseViewModel<Order>
    {
        public OrderListItemViewModel(Order order) : base(order)
        {
            Description = order.Name;
            TrackingNumber = order.TrackingNumber;
            Price = order.Price;
            PurchaseDate = order.PurchaseDate.ToShortDateString();
            ArrivalDate = order.ArrivalDate.ToShortDateString();

            GenerateItemPreview();
        }

        public string Description { get; set; }

        public string TrackingNumber { get; set; }

        public double Price { get; set; }

        public string PurchaseDate { get; set; }

        public string ArrivalDate { get; set; }

        public string ItemPreview { get; set; }

        public Color StatusColor => Utility.GetStatusColor(SourceObject.Status);

        private void GenerateItemPreview()
        {
            if (SourceObject.Items == null || SourceObject.Items.Count == 0)
                return;

            int itemCount = SourceObject.Items.Count > 2 ? 2 : SourceObject.Items.Count;
            for (int a = 0; a < itemCount; a++)
                ItemPreview += SourceObject.Items[a].Description + Environment.NewLine;

            if (SourceObject.Items.Count > 2)
                ItemPreview += $"and {SourceObject.Items.Count - 2} more item(s)...";

            ItemPreview = ItemPreview.Trim();
        }
    }
}
