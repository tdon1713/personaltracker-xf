﻿using PersonalTrackerXF.Models;

namespace PersonalTrackerXF.ViewModels
{
    public class OrderItemListItemViewModel : BaseViewModel<OrderItem>
    {
        private string _description;
        private string _url;
        private double _price;

        public OrderItemListItemViewModel() : base(null) {  }

        public OrderItemListItemViewModel(OrderItem item) : base(item)
        {
            if (item == null)
                return;

            Description = item.Description;
            Url = item.Url;
            Price = item.Price;
        }

        public OrderItemListItemViewModel(OrderItemListItemViewModel item) : base(item.SourceObject)
        {
            Description = item.Description;
            Url = item.Url;
            Price = item.Price;
        }

        public string Description
        {
            get => _description;
            set => SetValue(ref _description, value);
        }

        public string Url
        {
            get => _url;
            set => SetValue(ref _url, value);
        }

        public double Price
        {
            get => _price;
            set => SetValue(ref _price, value);
        }
    }
}
