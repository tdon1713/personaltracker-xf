﻿using PersonalTrackerXF.Helper;
using PersonalTrackerXF.Models;
using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace PersonalTrackerXF.ViewModels
{
    public class OrderItemViewModel : BaseViewModel<Order>
    {
        private string _name;
        private EnumerationViewModel _status;
        private string _trackingNumber;
        private string _url;
        private double _price;
        private DateTime _purchaseDate = DateTime.Now.Date;
        private DateTime _arrivalDate = DateTime.Now.Date.AddDays(2);
        private ObservableCollection<OrderItemListItemViewModel> _items;

        public OrderItemViewModel(Order order) : base(order)
        {
            if (order == null)
                return;    

            Name = order.Name;
            TrackingNumber = order.TrackingNumber;
            Url = order.Url;
            Price = order.Price == 0 ? 0.00 : order.Price;
            PurchaseDate = order.PurchaseDate;
            ArrivalDate = order.ArrivalDate;
            Items = order.Items.Select(item => new OrderItemListItemViewModel(item)).ToObsCollection();
        }

        public string Name
        {
            get => _name;
            set => SetValue(ref _name, value);
        }
        
        public EnumerationViewModel Status
        {
            get => _status;
            set => SetValue(ref _status, value);
        }

        public string TrackingNumber
        {
            get => _trackingNumber;
            set => SetValue(ref _trackingNumber, value);
        }

        public string Url
        {
            get => _url;
            set => SetValue(ref _url, value);
        }

        public double Price
        {
            get => _price;
            set => SetValue(ref _price, value);
        }
        
        public DateTime PurchaseDate
        {
            get => _purchaseDate;
            set => SetValue(ref _purchaseDate, value);
        }

        public DateTime ArrivalDate
        {
            get => _arrivalDate;
            set => SetValue(ref _arrivalDate, value);
        }

        public ObservableCollection<OrderItemListItemViewModel> Items
        {
            get => _items ?? (_items = new ObservableCollection<OrderItemListItemViewModel>());
            set => SetValue(ref _items, value);
        }
    }
}
