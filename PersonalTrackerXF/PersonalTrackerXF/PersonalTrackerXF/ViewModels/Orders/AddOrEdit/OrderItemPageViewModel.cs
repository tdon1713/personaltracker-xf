﻿using PersonalTrackerXF.Helper;
using PersonalTrackerXF.Logic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace PersonalTrackerXF.ViewModels
{
    public class OrderItemPageViewModel : BasePageViewModel
    {
        private OrderListItemViewModel _sourceOrder;

        private OrderItemViewModel _order;
        public OrderItemViewModel Order
        {
            get => _order ?? (_order = new OrderItemViewModel(null));
            set
            {
                SetValue(ref _order, value);
                OnPropertyChanged(nameof(AreItemsAvailable));
            }
        }

        private OrderItemListItemViewModel _orderItem;
        public OrderItemListItemViewModel OrderItem
        {
            get => _orderItem ?? (_orderItem = new OrderItemListItemViewModel());
            set
            {
                SetValue(ref _orderItem, value);
            }
        }

        private ObservableCollection<EnumerationViewModel> _statuses;
        public ObservableCollection<EnumerationViewModel> Statuses
        {
            get => _statuses ?? (_statuses = new ObservableCollection<EnumerationViewModel>(EnumExtensions.ListFrom<OrderStatus>()));
        }

        private bool _isAddToggled;
        public bool IsAddToggled
        {
            get => _isAddToggled;
            set
            {
                SetValue(ref _isAddToggled, value);
                OnPropertyChanged(nameof(ToggleItemIcon));
            }
        }

        public bool AreItemsAvailable
        {
            get => Order.SourceObject != null;
        }

        public string ToggleItemIcon
        {
            get
            {
                if (IsAddToggled)
                {
                    return Constants.Resources.FontAwesome.Minus;
                }

                return Constants.Resources.FontAwesome.Plus;
            }
        }

        public bool IsItemSelected
        {
            get => OrderItem?.SourceObject != null || OrderItem?.SourceObject?.ID > 0;
        }

        private ICommand _saveCommand;
        public ICommand SaveCommand => _saveCommand ?? (_saveCommand = new Command(() => Save()));

        private ICommand _deleteCommand;
        public ICommand DeleteCommand => _deleteCommand ?? (_deleteCommand = new Command(() => Delete()));

        private ICommand _toggleAddItemCommand;
        public ICommand ToggleAddItemCommand => _toggleAddItemCommand ?? (_toggleAddItemCommand = new Command(() => IsAddToggled = !IsAddToggled));

        private ICommand _saveItemCommand;
        public ICommand SaveItemCommand => _saveItemCommand ?? (_saveItemCommand = new Command(() => SaveItem()));

        private ICommand _itemTappedCommand;
        public ICommand ItemTappedCommand => _itemTappedCommand ?? (_itemTappedCommand = new Command<OrderItemListItemViewModel>((item) => ItemTapped(item)));

        private ICommand _refreshItemCommand;
        public ICommand RefreshItemCommand => _refreshItemCommand ?? (_refreshItemCommand = new Command(() => RefreshItem()));

        private ICommand _deleteItemCommand;
        public ICommand DeleteItemCommand => _deleteItemCommand ?? (_deleteItemCommand = new Command(() => DeleteItem()));

        private ICommand _trackItemCommand;
        public ICommand TrackItemCommand => _trackItemCommand ?? (_trackItemCommand = new Command(() => TrackItem()));

        private ICommand _openOrderItemUrlCommand;
        public ICommand OpenOrderItemUrlCommand => _openOrderItemUrlCommand ?? (_openOrderItemUrlCommand = new Command(() => OpenOrderItemUrl()));

        private ICommand _openOrderUrlCommand;
        public ICommand OpenOrderUrlCommand => _openOrderUrlCommand ?? (_openOrderUrlCommand = new Command(() => OpenOrderUrl()));

        public OrderItemPageViewModel(OrderListItemViewModel order)
        {
            _sourceOrder = order;
        }

        public override async Task InitAsync()
        {
            await base.InitAsync();

            if (_sourceOrder?.SourceObject != null)
            {
                var order = await OrderLogic.Get(_sourceOrder.SourceObject.ID);
                Order = new OrderItemViewModel(order);
                Order.Status = Statuses.FirstOrDefault(item => item.Value == (int)order.Status);
            }
            else
            {
                Order.Status = Statuses.First(item => item.Value == (int)OrderStatus.Ordered);
            }
        }

        private async void Delete()
        {
            if (Order.SourceObject == null)
                return;

            bool delete = await ShowConfirm(AppResource.ConfirmHeaderAreYouSure, string.Format(AppResource.ConfirmDeleteOrder, Order.SourceObject.Name));
            if (!delete)
                return;

            ToggleLoading();

            await OrderLogic.Delete(Order.SourceObject);
            await Paging.PopDetailAsync();

            ToggleLoading();
        }

        private async void DeleteItem()
        {
            if (OrderItem.SourceObject == null)
                return;

            bool delete = await ShowConfirm(AppResource.ConfirmHeaderAreYouSure, string.Format(AppResource.ConfirmDeleteOrderItem, OrderItem.SourceObject.Description));
            if (!delete)
                return;

            ToggleLoading();

            await OrderLogic.Delete(OrderItem.SourceObject);
            await RefreshItems();
            RefreshItem();

            ToggleLoading();
        }

        private void ItemTapped(OrderItemListItemViewModel item)
        {
            if (item == null)
                return;

            OrderItem = item.Copy();
            OrderItem.SourceObject = item.SourceObject.Copy();
            OrderItem.SourceObject.ID = item.SourceObject.ID;

            IsAddToggled = true;
            OnPropertyChanged(nameof(IsItemSelected));
        }
        
        private void RefreshItem()
        {
            OrderItem = new OrderItemListItemViewModel();
            OnPropertyChanged(nameof(IsItemSelected));
        }

        private async Task RefreshItems()
        {
            var items = await OrderLogic.GetItems(Order.SourceObject.ID);
            Order.Items = items.Select(item => new OrderItemListItemViewModel(item)).ToObsCollection();
        }

        private async void Save()
        {
            ToggleLoading();

            Order = await OrderLogic.Save(Order);

            ToggleLoading();
        }

        private async void SaveItem()
        {
            ToggleLoading();

            await OrderLogic.Save(OrderItem, Order.SourceObject.ID);
            await RefreshItems();

            RefreshItem();

            ToggleLoading();
        }

        private void TrackItem()
        {
            if (string.IsNullOrEmpty(Order.TrackingNumber))
                return;

            Utility.OpenUrl($"https://www.google.com/search?q={Order.TrackingNumber}");
        }

        private void OpenOrderItemUrl()
        {
            if (string.IsNullOrWhiteSpace(OrderItem?.Url))
                return;

            Utility.OpenUrl(OrderItem.Url);
        }

        private void OpenOrderUrl()
        {
            if (string.IsNullOrEmpty(Order.Url))
                return;

            Utility.OpenUrl(Order.Url);
        }
    }
}
