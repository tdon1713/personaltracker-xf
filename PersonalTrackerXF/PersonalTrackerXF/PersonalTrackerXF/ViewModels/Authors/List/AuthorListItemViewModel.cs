﻿using PersonalTrackerXF.Models;

namespace PersonalTrackerXF.ViewModels
{
    public class AuthorListItemViewModel : BaseViewModel<Author>
    {
        public AuthorListItemViewModel(Author author) : base(author)
        {
            Name = $"{author.FirstName} {author.LastName}";
            BookCount = author.BookCount;
        }

        public string Name { get; set; }

        public int BookCount { get; set; }
    }
}
