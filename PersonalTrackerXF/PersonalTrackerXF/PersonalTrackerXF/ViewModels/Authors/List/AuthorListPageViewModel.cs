﻿using PersonalTrackerXF.Helper;
using PersonalTrackerXF.Logic;
using PersonalTrackerXF.Pages;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace PersonalTrackerXF.ViewModels
{
    public class AuthorListPageViewModel : BasePageViewModel
    {
        private ObservableCollection<AuthorListItemViewModel> _authors;
        public ObservableCollection<AuthorListItemViewModel> Authors
        {
            get => _authors;
            set => SetValue(ref _authors, value);
        }

        private string _searchText;
        public string SearchText
        {
            get => _searchText;
            set => SetValue(ref _searchText, value);
        }

        private ICommand _itemTappedCommand;
        public ICommand ItemTappedCommand => _itemTappedCommand ?? (_itemTappedCommand = new Command<AuthorListItemViewModel>((item) => ItemTapped(item)));

        private ICommand _updateStatsCommand;
        public ICommand UpdateStatsCommand => _updateStatsCommand ?? (_updateStatsCommand = new Command(() => UpdateStats()));

        private ICommand _searchCommand;
        public ICommand SearchCommand => _searchCommand ?? (_searchCommand = new Command(async () => await Search()));

        public AuthorListPageViewModel() { }

        public override async Task InitAsync()
        {
            await base.InitAsync();
            await Search();
        }

        private async void ItemTapped(AuthorListItemViewModel item)
        {
            if (item == null)
                return;

            await Paging.PushDetailAsync(new AuthorItemPage(item));
        }

        public async Task Search()
        {
            ToggleLoading();
            var authors = await AuthorLogic.GetAsync();
            if (!string.IsNullOrWhiteSpace(SearchText))
            {
                authors = authors.Where(item => item.Name.ToUpper().Contains(SearchText.ToUpper())).ToList();
            }

            Authors = authors.OrderBy(item => item.FirstName)
                   .ThenBy(item => item.LastName)
                   .Select(item => new AuthorListItemViewModel(item))
                   .ToObsCollection();

            ToggleLoading();
        }

        private async void UpdateStats()
        {
            var confirm = await ShowConfirm(AppResource.ConfirmHeaderAreYouSure, AppResource.ConfirmUpdateAuthorStats);
            if (!confirm)
                return;

            ToggleLoading();

            await AuthorLogic.UpdateStats(Authors.Select(item => item.SourceObject));
            await InitAsync();

            ToggleLoading();
        }
    }
}
