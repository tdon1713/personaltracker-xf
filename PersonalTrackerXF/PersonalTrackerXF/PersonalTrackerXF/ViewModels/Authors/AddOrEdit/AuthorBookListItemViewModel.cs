﻿using PersonalTrackerXF.Helper;
using PersonalTrackerXF.Models;

namespace PersonalTrackerXF.ViewModels
{
    public class AuthorBookListItemViewModel : BaseViewModel<Book>
    {
        public AuthorBookListItemViewModel(Book book) : base(book)
        {
            BookTitle = book.Title;
            if (book.Volume != 0)
                BookTitle += $", Vol. {book.Volume}";

            Type = book.Type.ToUIString();
            Status = book.Status.ToUIString();
        }

        public string BookTitle { get; set; } 

        public string Type { get; set; }

        public string Status { get; set; }
    }
}
