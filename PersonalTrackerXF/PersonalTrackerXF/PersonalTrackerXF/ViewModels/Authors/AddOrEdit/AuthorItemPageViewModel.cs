﻿using PersonalTrackerXF.Helper;
using PersonalTrackerXF.Logic;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace PersonalTrackerXF.ViewModels
{
    public class AuthorItemPageViewModel : BasePageViewModel
    {
        private AuthorListItemViewModel _selectedAuthor;

        private AuthorItemViewModel _item;
        public AuthorItemViewModel Item
        {
            get => _item;
            set => SetValue(ref _item, value);
        }

        private ICommand _saveAuthor;
        public ICommand SaveAuthor => _saveAuthor ?? (_saveAuthor = new Command(() => Save()));

        private ICommand _deleteAuthor;
        public ICommand DeleteAuthor => _deleteAuthor ?? (_deleteAuthor = new Command(() => Delete()));

        private ObservableCollection<AuthorBookListItemViewModel> _books;
        public ObservableCollection<AuthorBookListItemViewModel> Books
        {
            get => _books;
            set => SetValue(ref _books, value);
        }

        public AuthorItemPageViewModel(AuthorListItemViewModel author)
        {
            _selectedAuthor = author;
        }

        public override async Task InitAsync()
        {
            await base.InitAsync();

            ToggleLoading();

            if (_selectedAuthor == null)
            {
                Item = new AuthorItemViewModel(null);
            }
            else
            {
                var author = await AuthorLogic.GetAsync(_selectedAuthor.SourceObject.ID);
                Item = new AuthorItemViewModel(author);

                var books = await BookLogic.GetForAuthor(_selectedAuthor.SourceObject.ID);
                Books = books.Select(item => new AuthorBookListItemViewModel(item)).ToObsCollection();
            }

            ToggleLoading();
        }

        private async void Delete()
        {
            if (Item.SourceObject == null)
                return;

            bool delete = await ShowConfirm(AppResource.ConfirmHeaderAreYouSure, String.Format(AppResource.ConfirmDeleteAuthor, Item.SourceObject.Name));
            if (!delete)
                return;

            ToggleLoading();

            await AuthorLogic.DeleteAsync(Item.SourceObject);
            await Paging.PopDetailAsync();

            ToggleLoading();
        }

        private async void Save()
        {
            ToggleLoading();

            try
            {
                Item = await AuthorLogic.Save(Item);
                await Paging.PopDetailAsync();
            }
            catch (ArgumentException aex)
            {
                await ShowAlert(aex.Message, isError: true);
            }
            finally
            {
                ToggleLoading();
            }
        }
    }
}
