﻿using PersonalTrackerXF.Models;

namespace PersonalTrackerXF.ViewModels
{
    public class AuthorItemViewModel : BaseViewModel<Author>
    {
        private string _firstName;
        private string _lastName;

        public AuthorItemViewModel(Author author) : base(author)
        {
            if (author == null)
                return;

            FirstName = author.FirstName;
            LastName = author.LastName;
        }

        public string FirstName
        {
            get => _firstName;
            set => SetValue(ref _firstName, value);
        }

        public string LastName
        {
            get => _lastName;
            set => SetValue(ref _lastName, value);
        }
    }
}
