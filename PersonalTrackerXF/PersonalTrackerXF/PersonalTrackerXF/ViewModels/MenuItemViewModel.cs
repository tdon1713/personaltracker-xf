﻿using Xamarin.Forms;

namespace PersonalTrackerXF.ViewModels
{
    public class MenuItemViewModel
    {
        public Helper.MenuItem Item { get; set; }

        public string FAIcon { get; set; }

        public string Title { get; set; }

        public bool IsChild { get; set; } = false;

        public Thickness Padding
        {
            get
            {
                if (IsChild)
                    return new Thickness(30, 10, 0, 10);

                return new Thickness(0, 10);
            }
        }
    }
}
