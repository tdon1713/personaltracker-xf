﻿using Microcharts;
using PersonalTrackerXF.Helper;
using PersonalTrackerXF.Logic;
using PersonalTrackerXF.Pages;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace PersonalTrackerXF.ViewModels
{
    public class OverviewPageViewModel : BasePageViewModel
    {
        #region Created Books
        private bool _createdBooksChartLoading = true;
        public bool CreatedBooksChartLoading
        {
            get => _createdBooksChartLoading;
            set => SetValue(ref _createdBooksChartLoading, value);
        }

        private Chart _createdBooksChart;
        public Chart CreatedBooksChart
        {
            get => _createdBooksChart;
            private set => SetValue(ref _createdBooksChart, value);
        }
        #endregion

        #region Completed Books
        private bool _completedBooksChartLoading = true;
        public bool CompletedBooksChartLoading
        {
            get => _completedBooksChartLoading;
            set => SetValue(ref _completedBooksChartLoading, value);
        }

        private Chart _completedBooksChart;
        public Chart CompletedBooksChart
        {
            get => _completedBooksChart;
            private set => SetValue(ref _completedBooksChart, value);
        }
        #endregion

        #region Todays Orders
        private bool _todaysOrdersLoading = true;
        public bool TodaysOrdersLoading
        {
            get => _todaysOrdersLoading;
            set => SetValue(ref _todaysOrdersLoading, value);
        }

        private ObservableCollection<OrderListItemViewModel> _todaysOrders;
        public ObservableCollection<OrderListItemViewModel> TodaysOrders
        {
            get => _todaysOrders ?? (_todaysOrders = new ObservableCollection<OrderListItemViewModel>());
            set => SetValue(ref _todaysOrders, value);
        }
        #endregion

        #region Upcoming Orders
        private bool _upcomingOrdersLoading = true;
        public bool UpcomingOrdersLoading
        {
            get => _upcomingOrdersLoading;
            set => SetValue(ref _upcomingOrdersLoading, value);
        }

        private ObservableCollection<OrderListItemViewModel> _upcomingOrders;
        public ObservableCollection<OrderListItemViewModel> UpcomingOrders
        {
            get => _upcomingOrders ?? (_upcomingOrders = new ObservableCollection<OrderListItemViewModel>());
            set => SetValue(ref _upcomingOrders, value);
        }
        #endregion

        #region Shared Orders

        private ICommand _orderSelectedCommand;
        public ICommand OrderSelectedCommand => _orderSelectedCommand ?? (_orderSelectedCommand = new Command<OrderListItemViewModel>((item) => OrderSelected(item)));

        #endregion

        public OverviewPageViewModel() { }

        public override async Task InitAsync()
        {
            await base.InitAsync();
            LoadCreatedBooksChart();
            LoadCompletedBooksChart();
            LoadTodaysOrders();
            LoadUpcomingOrders();
        }

        private void LoadCreatedBooksChart() => Task.Run(async () => {
            CreatedBooksChart = await BookLogic.GetRecentlyCreatedChart();
            CreatedBooksChartLoading = false;
        });

        private void LoadCompletedBooksChart() => Task.Run(async () => {
            CompletedBooksChart = await BookLogic.GetRecentlyCompletedChart();
            CompletedBooksChartLoading = false;
        });

        private void LoadTodaysOrders() => Task.Run(async () => {
            var orders = await OrderLogic.GetToday();
            var collection = new ObservableCollection<OrderListItemViewModel>();
            foreach (var order in orders)
            {
                collection.Add(new OrderListItemViewModel(order));
            }

            TodaysOrders = collection;
            TodaysOrdersLoading = false;
        });

        private void LoadUpcomingOrders() => Task.Run(async () => {
            var orders = await OrderLogic.GetUpcoming();
            var collection = new ObservableCollection<OrderListItemViewModel>();
            foreach (var order in orders)
            {
                collection.Add(new OrderListItemViewModel(order));
            }

            UpcomingOrders = collection;
            UpcomingOrdersLoading = false;
        });

        private async void OrderSelected(OrderListItemViewModel item)
        {
            if (item == null)
                return;

            await Paging.PushDetailAsync(new OrderItemPage(item));
        }
    }
}
