﻿using PersonalTrackerXF.Helper;
using PersonalTrackerXF.ServiceInterfaces;
using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Windows.Storage;
using Xamarin.Forms;

[assembly: Dependency(typeof(PersonalTrackerXF.UWP.Services.FileHelper))]

namespace PersonalTrackerXF.UWP.Services
{
    public class FileHelper : IFileHelper
    {
        public async Task ClearFolder(Subfolders folder)
        {
            var subFolder = await GetFolder(folder);
            await subFolder.DeleteAsync(StorageDeleteOption.PermanentDelete);
        }

        public async Task<(bool success, string filePath)> Download(string url, Subfolders subFolder, SupportedFileType fileType)
        {
            if (string.IsNullOrEmpty(url))
                throw new ArgumentNullException(nameof(url));
            
            StorageFolder folder = await GetFolder(subFolder);
            string fileName = $"{Guid.NewGuid().ToString()}.{fileType.ToString().ToLower()}";
            string filePath;

            try
            {
                var file = await folder.CreateFileAsync(fileName, CreationCollisionOption.FailIfExists);

                HttpClient client = new HttpClient();
                byte[] buffer = await client.GetByteArrayAsync(url);

                using (Stream stream = await file.OpenStreamForWriteAsync())
                    stream.Write(buffer, 0, buffer.Length);

                filePath = file.Path;
            }
            catch (Exception ex)
            {
                return (false, string.Empty);
            }

            return (true, filePath);
        }

        public string GetRoot()
        {
            return ApplicationData.Current.LocalFolder.Path;
        }

        public async Task<string> GetPath(Subfolders subFolder)
        {
            StorageFolder folder = await GetFolder(subFolder);
            return folder.Path;
        }

        private async Task<StorageFolder> GetFolder(Subfolders subFolder)
        {
            StorageFolder folder;
            switch (subFolder)
            {
                case Subfolders.Covers_Books:
                    folder = await ApplicationData.Current.LocalFolder.CreateFolderAsync(@"Covers\Books", CreationCollisionOption.OpenIfExists);
                    break;
                case Subfolders.Covers_Books_Temp:
                    folder = await ApplicationData.Current.LocalFolder.CreateFolderAsync(@"Covers\Books\Temp", CreationCollisionOption.OpenIfExists);
                    break;
                default:
                    folder = ApplicationData.Current.LocalFolder;
                    break;
            }

            return folder;
        }

        public async Task<string> Move(string location, Subfolders subFolder)
        {
            var file = await StorageFile.GetFileFromPathAsync(location);
            var newFolder = await GetFolder(subFolder);
            await file.MoveAsync(newFolder);

            return file.Path;
        }
    }
}
