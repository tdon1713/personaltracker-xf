﻿using PersonalTrackerXF.UWP;
using Xamarin.Forms.Platform.UWP;

[assembly: ExportRenderer(typeof(DLToolkit.Forms.Controls.FlowListView), typeof(FlowListViewRenderer))]

namespace PersonalTrackerXF.UWP
{
    public class FlowListViewRenderer : ListViewRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.ListView> e)
        {
            base.OnElementChanged(e);

            if (List != null)
                List.SelectionMode = Windows.UI.Xaml.Controls.ListViewSelectionMode.None;
        }
    }
}
