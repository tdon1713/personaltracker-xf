﻿using PersonalTrackerXF.UWP;
using Windows.UI.Xaml.Media;
using Xamarin.Forms;
using Xamarin.Forms.Platform.UWP;

[assembly: ExportRenderer(typeof(Frame), typeof(CustomFrameRenderer))]

namespace PersonalTrackerXF.UWP
{
    public class CustomFrameRenderer : FrameRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Frame> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                var frame = e.NewElement;
                Windows.UI.Color frameBG = Windows.UI.Color.FromArgb(
                    (byte)(frame.BackgroundColor.A * 255),
                    (byte)(frame.BackgroundColor.R * 255),
                    (byte)(frame.BackgroundColor.G * 255),
                    (byte)(frame.BackgroundColor.B * 255));

                Control.CornerRadius = new Windows.UI.Xaml.CornerRadius(frame.CornerRadius);
                Control.Background = new SolidColorBrush(frameBG);
                frame.BackgroundColor = Color.Transparent;
            }
        }
    }
}
