﻿using System.Threading.Tasks;
using Windows.ApplicationModel.Background;

namespace PersonalTrackerXF.UWP
{
    public sealed partial class MainPage
    {
        public MainPage()
        {
            this.InitializeComponent();

            LoadApplication(new PersonalTrackerXF.App());
        }
    }
}
